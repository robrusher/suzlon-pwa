# Suzlon Secure Node.js App with SQL Server

Built using:

* [Node.js](https://nodejs.org/en/)
* [SQL Server](https://www.microsoft.com/en-us/sql-server/sql-server-2017)
* [hapi](https://hapijs.com/)
* [Vue.js](https://vuejs.org/)

## Requirements

* [Node.js](https://nodejs.org/en/) version 8.0 or higher
* [SQL Server](https://www.microsoft.com/en-us/sql-server/sql-server-2017) version 2012 or higher

## Set Up Local Development Environment

1. Clone this repository (or download and extract the zip file)
2. Open a command prompt or terminal
3. Change to the directory that contains the project files
4. Run `npm install` to install all the dependencies

### Run the Local Web Application

1. Run `npm run dev` to start the development server
1. Browse to `http://localhost:8080`

### Run the Tests

```bash
> npm run test
```


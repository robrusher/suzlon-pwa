import Vue from "vue";
import App from "./App";
import router from "./router";

import Datetime from "vue-datetime";
import "vue-datetime/dist/vue-datetime.css";
import "materialize-css";
import "materialize-css/dist/css/materialize.min.css";


Vue.use( Datetime );

new Vue( {
  router,
  render( h ) {
    return h( App );
  }
} ).$mount( "#app" );

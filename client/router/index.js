import Vue from "vue";
import VueRouter from "vue-router";
import Sites from "../views/Sites";
import Site from "../views/Site";
import Turbine from "../views/Turbine";

Vue.use( VueRouter );

const routes = [
  {
    path: "/",
    name: "sites",
    component: Sites
  },
  {
    path: "/site/:id",
    name: "site",
    component: Site
  },
  {
    path: "/turbine/:id",
    name: "turbine",
    component: Turbine
  }
];

const router = new VueRouter( {
  routes
} );

export default router;

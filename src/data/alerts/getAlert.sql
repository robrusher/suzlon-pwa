SELECT [Turbine_ID],
	[Plant],
	[Turbine],
	[StartTime],
	[EndTime],
	[Duration],
	[Event],
	[Description],
	[WindSpeed],
	[Temp],
	[LostProduction],
	[LastReset],
	[NumberOfResets] 
FROM   [dbo].[TAL_Activity]
WHERE  0 = 0 AND
       Turbine_ID = @alertID;
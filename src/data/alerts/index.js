"use strict";

const utils = require( "../utils" );

const register = async ( { sql, getConnection } ) => {
    // read in all the .sql files for this folder
    const sqlQueries = await utils.loadSqlQueries( "alerts" );

    const getAlerts = async recCnt => {
        // get a connection to SQL Server
        const cnx = await getConnection();

        // create a new request
        const request = await cnx.request();

        // configure sql query parameters
        request.input( "recCnt", sql.Int, recCnt );

        // return the executed query
        return request.query( sqlQueries.getAlerts );
    };

    const getAlert = async alertID => {
    // get a connection to SQL Server
        const cnx = await getConnection();

        // create a new request
        const request = await cnx.request();

        // configure sql query parameters
        request.input( "alertID", sql.Int, alertID );

        // return the executed query
        return request.query( sqlQueries.getAlert );
    };

    return {
        getAlerts,
        getAlert
    };
};

module.exports = { register };

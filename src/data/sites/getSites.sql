SELECT [id]
      ,[site_name]
      ,[region_name]
      ,[num_turbines]
      ,[city]
      ,[state]
      ,[company_name]
      ,[cod_date]
      ,[trueup_date]      
FROM [dbo].[vSites]
WHERE   0 = 0
ORDER BY site_name;
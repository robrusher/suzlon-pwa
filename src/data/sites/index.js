"use strict";

const utils = require( "../utils" );

const register = async ( { sql, getConnection } ) => {
    // read in all the .sql files for this folder
    const sqlQueries = await utils.loadSqlQueries( "sites" );

    const getSites = async recCnt => {
        // get a connection to SQL Server
        const cnx = await getConnection();

        // create a new request
        const request = await cnx.request();

        // configure sql query parameters
        request.input( "recCnt", sql.Int, recCnt );

        // return the executed query
        return request.query( sqlQueries.getSites );
    };

    const getSite = async siteId => {
        const cnx = await getConnection();
        const request = await cnx.request();

        // configure sql query parameters
        request.input( "siteId", sql.Int, siteId );

        // return the executed query
        return request.query( sqlQueries.getSite );
    };

    return {
        getSites,
        getSite
    };
};

module.exports = { register };

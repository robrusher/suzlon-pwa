"use strict";

const utils = require( "../utils" );

const register = async ( { sql, getConnection } ) => {
    // read in all the .sql files for this folder
    const sqlQueries = await utils.loadSqlQueries( "turbine" );

    const getTurbine = async turbineId => {
        const cnx = await getConnection();
        const request = await cnx.request();

        // configure sql query parameters
        request.input( "turbineId", sql.Int, turbineId );

        // return the executed query
        return request.query( sqlQueries.getTurbine );
    };

    return {
        getTurbine
    };
};

module.exports = { register };

"use strict";

const bell = require( "@hapi/bell" );
const authCookie = require( "@hapi/cookie" );

const isSecure = process.env.NODE_ENV === "production";

module.exports.register = async server => {
    // register plugins
    const config = server.app.config;
    await server.register( [ authCookie, bell ] );

    // configure cookie authorization strategy
    server.auth.strategy( "session", "cookie", {
        cookie: {
            name: "smcapp",
            password: config.cookiePwd,
            isSecure // Should be set to true (which is the default) in production
        },
        redirectTo: "/authorization-code/callback" // If there is no session, redirect here
    } );

    // configure bell to use your Azure authorization server
    server.auth.strategy( "azure", "bell", {
        provider: "azure",
        config: { tenant: config.auth.tenantId },
        password: config.auth.password,
        clientId: config.auth.clientId,
        clientSecret: config.cookiePwd,
        providerParams: {
            display: "popup",
            tenant: config.auth.tenantId,
            responseType: "code"
        },
        scope: [ "openid", "offline_access", "profile", "user.read" ],
        isSecure: false
    } );
};

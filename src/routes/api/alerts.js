"use strict";
const boom = require( "@hapi/boom" );

module.exports.register = async server => {
    server.route( {
        method: "GET",
        path: "/api/alerts",
        config: {
            handler: async request => {
                try {
                    // get the sql client registered as a plugin
                    const db = request.server.plugins.sql.client;

                    // TODO: adjust parameters
                    const recordCnt = 100;

                    // execute the query
                    const res = await db.alerts.getAlerts( recordCnt );

                    // return the recordset object
                    return res.recordset;
                } catch ( err ) {
                    server.log( [ "error", "api", "events" ], err );
                    return boom.boomify( err );
                }
            }
        }
    } );

    server.route( {
        method: "GET",
        path: "/api/alerts/{id}",
        config: {
            handler: async request => {
                try {
                // get the sql client registered as a plugin
                    const db = request.server.plugins.sql.client;

                    // TODO: adjust parameters
                    const id = request.param.id;

                    // execute the query
                    const res = await db.alerts.getAlert( id );

                    // return the recordset object
                    return res.recordset;
                } catch ( err ) {
                    server.log( [ "error", "api", "events" ], err );
                    return boom.boomify( err );
                }
            }
        }
    } );
};

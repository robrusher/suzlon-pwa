"use strict";

const alerts = require( "./alerts" );
const anomalies = require( "./anomalies" );
const conditions = require( "./conditions" );
const costs = require( "./costs" );
const formz = require( "./formz" );
const lookups = require( "./lookups" );
const orders = require( "./orders" );
const parts = require( "./parts" );
const turbine = require( "./turbine" );
const sites = require( "./sites" );

module.exports.register = async server => {
    await alerts.register( server );
    await anomalies.register( server );
    await conditions.register( server );
    await costs.register( server );
    await formz.register( server );
    await lookups.register( server );
    await orders.register( server );
    await parts.register( server );
    await turbine.register( server );
    await sites.register( server );
};

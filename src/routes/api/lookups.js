"use strict";
const boom = require( "@hapi/boom" );

module.exports.register = async server => {
    server.route( {
        method: "GET",
        path: "/api/lookups/accesslevel",
        config: {
            handler: async request => {
                try {
                    // get the sql client registered as a plugin
                    const db = request.server.plugins.sql.client;

                    // execute the query
                    const res = await db.lookups.getAccessLevels();

                    // return the recordset object
                    return res.recordset;
                } catch ( err ) {
                    server.log( [ "error", "api", "events" ], err );
                    return boom.boomify( err );
                }
            }
        }
    } );

    server.route( {
        method: "GET",
        path: "/api/lookups/active",
        config: {
            handler: async request => {
                try {
                    // get the sql client registered as a plugin
                    const db = request.server.plugins.sql.client;

                    // execute the query
                    const res = await db.lookups.getActiveStatus();

                    // return the recordset object
                    return res.recordset;
                } catch ( err ) {
                    server.log( [ "error", "api", "events" ], err );
                    return boom.boomify( err );
                }
            }
        }
    } );
    server.route( {
        method: "GET",
        path: "/api/lookups/blade",
        config: {
            handler: async request => {
                try {
                    // get the sql client registered as a plugin
                    const db = request.server.plugins.sql.client;

                    // execute the query
                    const res = await db.lookups.getBladeTypes();

                    // return the recordset object
                    return res.recordset;
                } catch ( err ) {
                    server.log( [ "error", "api", "events" ], err );
                    return boom.boomify( err );
                }
            }
        }
    } );
    server.route( {
        method: "GET",
        path: "/api/lookups/cms",
        config: {
            handler: async request => {
                try {
                    // get the sql client registered as a plugin
                    const db = request.server.plugins.sql.client;

                    // execute the query
                    const res = await db.lookups.getCMS();

                    // return the recordset object
                    return res.recordset;
                } catch ( err ) {
                    server.log( [ "error", "api", "events" ], err );
                    return boom.boomify( err );
                }
            }
        }
    } );
    server.route( {
        method: "GET",
        path: "/api/lookups/company",
        config: {
            handler: async request => {
                try {
                    // get the sql client registered as a plugin
                    const db = request.server.plugins.sql.client;

                    // execute the query
                    const res = await db.lookups.getCompanies();

                    // return the recordset object
                    return res.recordset;
                } catch ( err ) {
                    server.log( [ "error", "api", "events" ], err );
                    return boom.boomify( err );
                }
            }
        }
    } );
    server.route( {
        method: "GET",
        path: "/api/lookups/contact",
        config: {
            handler: async request => {
                try {
                    // get the sql client registered as a plugin
                    const db = request.server.plugins.sql.client;

                    // execute the query
                    const res = await db.lookups.getContacts();

                    // return the recordset object
                    return res.recordset;
                } catch ( err ) {
                    server.log( [ "error", "api", "events" ], err );
                    return boom.boomify( err );
                }
            }
        }
    } );
    server.route( {
        method: "GET",
        path: "/api/lookups/contactrole",
        config: {
            handler: async request => {
                try {
                    // get the sql client registered as a plugin
                    const db = request.server.plugins.sql.client;

                    // execute the query
                    const res = await db.lookups.getContactRoles();

                    // return the recordset object
                    return res.recordset;
                } catch ( err ) {
                    server.log( [ "error", "api", "events" ], err );
                    return boom.boomify( err );
                }
            }
        }
    } );
    server.route( {
        method: "GET",
        path: "/api/lookups/contacttype",
        config: {
            handler: async request => {
                try {
                    // get the sql client registered as a plugin
                    const db = request.server.plugins.sql.client;

                    // execute the query
                    const res = await db.lookups.getContactTypes();

                    // return the recordset object
                    return res.recordset;
                } catch ( err ) {
                    server.log( [ "error", "api", "events" ], err );
                    return boom.boomify( err );
                }
            }
        }
    } );
    server.route( {
        method: "GET",
        path: "/api/lookups/contract",
        config: {
            handler: async request => {
                try {
                    // get the sql client registered as a plugin
                    const db = request.server.plugins.sql.client;

                    // execute the query
                    const res = await db.lookups.getContracts();

                    // return the recordset object
                    return res.recordset;
                } catch ( err ) {
                    server.log( [ "error", "api", "events" ], err );
                    return boom.boomify( err );
                }
            }
        }
    } );
    server.route( {
        method: "GET",
        path: "/api/lookups/contractstanding",
        config: {
            handler: async request => {
                try {
                    // get the sql client registered as a plugin
                    const db = request.server.plugins.sql.client;

                    // execute the query
                    const res = await db.lookups.getContractStandings();

                    // return the recordset object
                    return res.recordset;
                } catch ( err ) {
                    server.log( [ "error", "api", "events" ], err );
                    return boom.boomify( err );
                }
            }
        }
    } );
    server.route( {
        method: "GET",
        path: "/api/lookups/contractstatus",
        config: {
            handler: async request => {
                try {
                    // get the sql client registered as a plugin
                    const db = request.server.plugins.sql.client;

                    // execute the query
                    const res = await db.lookups.getContractStatus();

                    // return the recordset object
                    return res.recordset;
                } catch ( err ) {
                    server.log( [ "error", "api", "events" ], err );
                    return boom.boomify( err );
                }
            }
        }
    } );
    server.route( {
        method: "GET",
        path: "/api/lookups/country",
        config: {
            handler: async request => {
                try {
                    // get the sql client registered as a plugin
                    const db = request.server.plugins.sql.client;

                    // execute the query
                    const res = await db.lookups.getCountries();

                    // return the recordset object
                    return res.recordset;
                } catch ( err ) {
                    server.log( [ "error", "api", "events" ], err );
                    return boom.boomify( err );
                }
            }
        }
    } );
    server.route( {
        method: "GET",
        path: "/api/lookups/faultgroup",
        config: {
            handler: async request => {
                try {
                    // get the sql client registered as a plugin
                    const db = request.server.plugins.sql.client;

                    // execute the query
                    const res = await db.lookups.getFaultGroups();

                    // return the recordset object
                    return res.recordset;
                } catch ( err ) {
                    server.log( [ "error", "api", "events" ], err );
                    return boom.boomify( err );
                }
            }
        }
    } );
    server.route( {
        method: "GET",
        path: "/api/lookups/voltage",
        config: {
            handler: async request => {
                try {
                    // get the sql client registered as a plugin
                    const db = request.server.plugins.sql.client;

                    // execute the query
                    const res = await db.lookups.getVoltages();

                    // return the recordset object
                    return res.recordset;
                } catch ( err ) {
                    server.log( [ "error", "api", "events" ], err );
                    return boom.boomify( err );
                }
            }
        }
    } );
    server.route( {
        method: "GET",
        path: "/api/lookups/turbinedetail",
        config: {
            handler: async request => {
                try {
                    // get the sql client registered as a plugin
                    const db = request.server.plugins.sql.client;

                    // execute the query
                    const res = await db.lookups.getTurbineDetails();

                    // return the recordset object
                    return res.recordset;
                } catch ( err ) {
                    server.log( [ "error", "api", "events" ], err );
                    return boom.boomify( err );
                }
            }
        }
    } );
    server.route( {
        method: "GET",
        path: "/api/lookups/model",
        config: {
            handler: async request => {
                try {
                    // get the sql client registered as a plugin
                    const db = request.server.plugins.sql.client;

                    // execute the query
                    const res = await db.lookups.getModels();

                    // return the recordset object
                    return res.recordset;
                } catch ( err ) {
                    server.log( [ "error", "api", "events" ], err );
                    return boom.boomify( err );
                }
            }
        }
    } );
    server.route( {
        method: "GET",
        path: "/api/lookups/productiontype",
        config: {
            handler: async request => {
                try {
                    // get the sql client registered as a plugin
                    const db = request.server.plugins.sql.client;

                    // execute the query
                    const res = await db.lookups.getProductionTypes();

                    // return the recordset object
                    return res.recordset;
                } catch ( err ) {
                    server.log( [ "error", "api", "events" ], err );
                    return boom.boomify( err );
                }
            }
        }
    } );
    server.route( {
        method: "GET",
        path: "/api/lookups/region",
        config: {
            handler: async request => {
                try {
                    // get the sql client registered as a plugin
                    const db = request.server.plugins.sql.client;

                    // execute the query
                    const res = await db.lookups.getRegions();

                    // return the recordset object
                    return res.recordset;
                } catch ( err ) {
                    server.log( [ "error", "api", "events" ], err );
                    return boom.boomify( err );
                }
            }
        }
    } );
    server.route( {
        method: "GET",
        path: "/api/lookups/sapsite",
        config: {
            handler: async request => {
                try {
                    // get the sql client registered as a plugin
                    const db = request.server.plugins.sql.client;

                    // execute the query
                    const res = await db.lookups.getSapSites();

                    // return the recordset object
                    return res.recordset;
                } catch ( err ) {
                    server.log( [ "error", "api", "events" ], err );
                    return boom.boomify( err );
                }
            }
        }
    } );
    server.route( {
        method: "GET",
        path: "/api/lookups/software",
        config: {
            handler: async request => {
                try {
                    // get the sql client registered as a plugin
                    const db = request.server.plugins.sql.client;

                    // execute the query
                    const res = await db.lookups.getSoftware();

                    // return the recordset object
                    return res.recordset;
                } catch ( err ) {
                    server.log( [ "error", "api", "events" ], err );
                    return boom.boomify( err );
                }
            }
        }
    } );
    server.route( {
        method: "GET",
        path: "/api/lookups/state",
        config: {
            handler: async request => {
                try {
                    // get the sql client registered as a plugin
                    const db = request.server.plugins.sql.client;

                    // execute the query
                    const res = await db.lookups.getStates();

                    // return the recordset object
                    return res.recordset;
                } catch ( err ) {
                    server.log( [ "error", "api", "events" ], err );
                    return boom.boomify( err );
                }
            }
        }
    } );
    server.route( {
        method: "GET",
        path: "/api/lookups/tempvariant",
        config: {
            handler: async request => {
                try {
                    // get the sql client registered as a plugin
                    const db = request.server.plugins.sql.client;

                    // execute the query
                    const res = await db.lookups.getTempVariants();

                    // return the recordset object
                    return res.recordset;
                } catch ( err ) {
                    server.log( [ "error", "api", "events" ], err );
                    return boom.boomify( err );
                }
            }
        }
    } );
    server.route( {
        method: "GET",
        path: "/api/lookups/turbine",
        config: {
            handler: async request => {
                try {
                    // get the sql client registered as a plugin
                    const db = request.server.plugins.sql.client;

                    // execute the query
                    const res = await db.lookups.getTurbines();

                    // return the recordset object
                    return res.recordset;
                } catch ( err ) {
                    server.log( [ "error", "api", "events" ], err );
                    return boom.boomify( err );
                }
            }
        }
    } );
};

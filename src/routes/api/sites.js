"use strict";
const boom = require( "@hapi/boom" );

module.exports.register = async server => {
    server.route( {
        method: "GET",
        path: "/api/sites",
        config: {
            handler: async request => {
                try {
                    // get the sql client registered as a plugin
                    const db = request.server.plugins.sql.client;

                    // TODO: adjust parameters
                    const recordCnt = 100;

                    // execute the query
                    const res = await db.sites.getSites( recordCnt );

                    // return the recordset object
                    return res.recordset;
                } catch ( err ) {
                    server.log( [ "error", "api", "sites" ], err );
                    return boom.boomify( err );
                }
            }
        }
    } );

    server.route( {
        method: "GET",
        path: "/api/sites/{id}",
        config: {
            handler: async request => {
                try {
                    const db = request.server.plugins.sql.client;
                    const siteId = request.params.id;

                    // execute the query
                    const res = await db.sites.getSite( siteId );

                    // return the recordset object
                    return res.recordset;
                } catch ( err ) {
                    server.log( [ "error", "api", "sites" ], err );
                    return boom.boomify( err );
                }
            }
        }
    } );
};

"use strict";
const boom = require( "@hapi/boom" );

module.exports.register = async server => {
    server.route( {
        method: "GET",
        path: "/api/turbine/{id}",
        config: {
            handler: async request => {
                try {
                    const db = request.server.plugins.sql.client;
                    const turbineId = request.params.id;

                    // execute the query
                    const res = await db.turbine.getTurbine( turbineId );

                    // return the recordset object
                    return res.recordset;
                } catch ( err ) {
                    server.log( [ "error", "api", "turbine" ], err );
                    return boom.boomify( err );
                }
            }
        }
    } );
};
